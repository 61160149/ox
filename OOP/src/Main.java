
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        Scanner kb = new Scanner(System.in);
        String ans;
        boolean f = true;
        while (f) {
            game.startGame();
            System.out.println("you want to playing agin?(y/n)");
            f = playAgin(kb.next());
            
        }

    }

    public static boolean playAgin(String ans) {
        Scanner kb = new Scanner(System.in);
        boolean f ;
        if (ans.equalsIgnoreCase("n")) {
           f = false;         
        } else if (ans.equalsIgnoreCase("y")) {
           f = true;
        } else {
            System.out.println("Please input y or n");
            System.out.println("you want to playing agin?(y/n)");
             f = playAgin(kb.next());           
        }
        return f;

    }

}
