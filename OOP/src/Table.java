
public class Table extends Game {

    String[][] data;

    public Table() {
        data = new String[][]{{"-", "-", "-"},{"-", "-", "-"},{"-", "-", "-"}};
    }

    public void setTable(Player player, int row, int col) {
        
        if (data[row][col].equals("-")) {
            data[row][col] = player.getPlayer();
        } else {
            player.deleteTurn();
            System.out.println("This row and col has already been used.");
        }
    }

    public boolean checkWin(Player player) {
        if (checkDraw(player) == true) {
            return true;
        } else if (checkX1() == true) {
            return true;
        } else if (checkX2() == true) {
            return true;
        }
        for (int i = 0; i < 3; i++) {
            if (checkRow(i) == true) {
                return true;
            } else if (checkCol(i) == true) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkDraw(Player player) {
        if (player.getTurn() == 9) {
            System.out.println("Draw !!!");
            return true;
        }
        return false;
    }

    public boolean checkRow(int row) {
        if (data[row][0].equals(data[row][1]) && data[row][0].equals(data[row][2]) && !data[row][0].equals( "-")) {
            return true;
        }
        return false;
    }

    public boolean checkCol(int col) {
        if (data[0][col].equals(data[1][col]) && data[0][col].equals(data[2][col]) && data[0][col] != "-") {
            return true;
        }
        return false;
    }

    public boolean checkX1() {
        if (data[0][0].equals(data[1][1]) && data[0][0].equals(data[2][2]) && data[0][0] != "-") {
            return true;
        }
        return false;
    }

    public boolean checkX2() {
        if (data[0][2].equals(data[1][1]) && data[0][2].equals(data[2][0]) && !data[0][2].equals("-")) {
            return true;
        }
        return false;
    }



    public void getTable() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print(data[row][col] + " ");
            }
            System.out.println();
        }
    }
}
