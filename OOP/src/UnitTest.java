import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UnitTest {

	@Test
	public void testShowWekicome() {
		Game g = new Game();
		g.showWelcome();
	}
	@Test
	public void testShowTurn() {
		Game g = new Game();
		Player player = new Player();
		g.showTurn(player);
	}
	@Test
	public void testGetTable() {
		Table t = new Table();
		t.getTable();
	}
	@Test
	void testGetPlayer() {
		Player player = new Player();
		System.out.println(player.getPlayer());
		
	}
	
	@Test
	public void testInput() {
		Game g = new Game();
		Table t = new Table();
		g.input(new Player(), "2", "2",new Table());
	}
	@Test
	public void testCheckIndexOutOfBound() {
		Game g = new Game();
		g.outArray("1", "4");
	}
	@Test
	public void testCheckCharacters() {
		Game g = new Game();
		g.isNot("A", "A");
	}
	@Test
	public void testShowWin() {
		Game g = new Game();
		Table table = new Table();
		Player player = new Player();
		g.showWin(player, true, table);
	}
	@Test
	public void testDraw() {
		Game g = new Game();
		Player player = new Player();
		Table table = new Table();
		player.turn=9;
		table.checkDraw(player);
	}
	@Test
	public void testTextEnd() {
		Game g = new Game();
		g.textEnd();
	}
}
