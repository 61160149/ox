
import java.util.Scanner;

public class OX {

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showBoard(String[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static boolean checkRow(int i, String[][] board) {
        if (board[i][0] == board[i][1] && board[i][0].equals(board[i][2]) && board[i][0] != "-") {
            return true;
        }
        return false;
    }

    private static boolean checkCol(int i, String[][] board) {
        if (board[0][i] == board[1][i] && board[0][i].equals(board[2][i]) && board[0][i] != "-") {
            return true;
        }
        return false;
    }

    private static boolean checkX1( String[][] board) {
        if (board[0][0] == board[1][1] && board[0][0].equals(board[2][2]) && board[0][0] != "-") {
            return true;
        }
        return false;
    }

    private static boolean checkX2(String[][] board) {
        if (board[0][2].equals(board[1][1]) && board[0][2].equals(board[2][0]) && !board[0][2].equals("-")) {
            return true;
        }
        return false;
    }
    
    public static void runturn(String[][] board){
        String player;
        
        boolean end = false;
        int turn = 0;
        while(!end){
            turn++;
            if(turn%2==1){
                player = "O";
                end = runGame(player,end,board,turn);
            }else{
                player = "X";
                end = runGame(player,end,board,turn);
            }
           
            showDraw(turn, end);                 
        }
    }
    
    
    public static boolean runGame(String player, boolean end,String[][] board,int turn){
        showTurn(player);
        inputRC(board,turn,player);
        showBoard(board);    
        
        end = checkWin(board);
        Win(player,end);
        return end;
    }
    

    public static void inputRC(String[][] board, int turn,String player) {
        Scanner kb = new Scanner(System.in);
        
        boolean check1 = true, check2 = true;
        String Inxrow = " ", Inxcol = " ";
        System.out.print("Please input row col : ");
        while (check1 || check2) {      
            
            check1 = checkInput(Inxrow = kb.next());           
            check2 = checkInput(Inxcol = kb.next());        
        }
        input(player, Integer.parseInt(Inxrow)-1, Integer.parseInt(Inxcol)-1, board, turn);
    }

    private static boolean checkInput(String number) {
        if (number.equals("1") || number.equals("2") || number.equals("3")) {
            return false;
        } else {
            System.out.println("Please enter only values number 1 to 3. ");
            System.out.print("Please input row col : ");
            return true;
        }
    }

    public static void input(String player, int row, int col, String[][] board, int turn) {
        if (board[row][col].equals("-")) {
            board[row][col] = player;
        } else {
            turn--;
            System.out.println("This row and col has already been used.");
        }

    }

    public static void showTurn(String player) {
        System.out.println(player + " turn");

    }

    public static void showDraw(int turn, boolean end) {
        if (turn == 9 && (!end)) {
            end = true;
            System.out.println("Draw");
        }
    }

    public static boolean checkWin(String[][] board) {      
        boolean end1,end2,end3,end4;     
            end1 = checkX1(board);
            end2 = checkX2(board);
            for (int i = 0; i < 3; i++) {
                end3 = checkRow(i, board);
                end4 = checkCol(i, board);
                if(end1 == true || end2 == true || end3 == true || end4 == true){
                    return true;
                }
            }     
        return false;
    }
    
    public static void Win(String player,boolean end) {
        if(end==true)
            System.out.println(player + " Win");
    }

    public static void main(String[] args) {
        String[][] board = new String[][]{{"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}};

        showWelcome();
        showBoard(board);
        runturn(board);

    }

}
